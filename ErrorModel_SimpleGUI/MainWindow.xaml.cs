﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Windows;
using ErrorModel.References;
using ErrorModel.Framework;
using ErrorModelTypes;
using ErrorModelTypes.AnglesAndDistances;
using ErrorModelTypes.Surveys;
using ErrorModelTypes.Positional;
using System.IO;
using Microsoft.Win32;
using Matrices;
using XMLHandler;
using ReportDataGenerator;


namespace ErrorModel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                outputTextBox.Text = "";

                Position slot = new Position(new VerticalDepth(0, DistanceUnit.Metres),
                    new Northing(0, DistanceUnit.Metres), new Easting(0, DistanceUnit.Metres));

                string UserDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AJCErrorModel";
                //UserDirectory += "\\Rev5Tests";

                string wellPathFileName = "ISCWSA1_WellPath.txt";
                WellReferenceData wellRefData;
                if (Well1Radio.IsChecked == true)
                {
                    wellPathFileName = UserDirectory + @"\ISCWSA1_WellPath.txt";
                    wellRefData = new WellReferenceData(50000.0,
                    new Declination(-4.0, AngleUnit.Degrees),
                    new Angle(72.0, AngleUnit.Degrees),
                    new Angle(60.0, AngleUnit.Degrees),
                     9.80665,
                     3.0);
                }
                else if (Well2Radio.IsChecked == true)
                {
                    //wellPathFileName = @"C:\Users\Andy McGregor\Documents\Visual Studio 2017\Projects\ErrorModel\ErrorModel\ISCWSA2_WellPath.txt";
                    wellPathFileName = UserDirectory + @"\ISCWSA2_WellPath.txt";
                    wellRefData = new WellReferenceData(48000.0,
                    new Declination(2.0, AngleUnit.Degrees),
                    new Angle(58.0, AngleUnit.Degrees),
                    new Angle(28.0, AngleUnit.Degrees),
                     9.80665);
                }
                else if (Well3Radio.IsChecked == true)
                {
                    //wellPathFileName = @"C:\Users\Andy McGregor\Documents\Visual Studio 2017\Projects\ErrorModel\ErrorModel\ISCWSA3_WellPath.txt";
                    wellPathFileName = UserDirectory + @"\ISCWSA3_WellPath.txt";
                    wellRefData = new WellReferenceData(61000.0,
                    new Declination(13.0, AngleUnit.Degrees),
                    new Angle(-70.0, AngleUnit.Degrees),
                    new Angle(40.0, AngleUnit.Degrees),
                     9.80665);
                }
                else if (LochNessRadio.IsChecked == true)
                {
                    wellPathFileName = @"C:\Users\Andy McGregor\Documents\Visual Studio 2017\Projects\ErrorModel\ErrorModel\LochNess_WellPath.txt";
                    wellRefData = new WellReferenceData(50529.0,
                    new Declination(-2.832, AngleUnit.Degrees),
                    new Angle(70.851, AngleUnit.Degrees),
                    new Angle(57.327134, AngleUnit.Degrees),
                     9.80665);
                }
                else
                {
                    //wellPathFileName = @"C:\Users\Andy McGregor\Documents\Visual Studio 2017\Projects\ErrorModel\ErrorModel\PioneerShackleford-10T20H.txt";

                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    openFileDialog1.InitialDirectory = "c:\\";
                    openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    openFileDialog1.RestoreDirectory = true;
                    openFileDialog1.Title = "Open Survey File";

                    if (openFileDialog1.ShowDialog() == true)
                        wellPathFileName = openFileDialog1.FileName;

                    double btotal;
                    double dip;
                    double dec;
                    double lat;
                    double gtotal;

                    if ((Double.TryParse(btotalTextBox.Text, out btotal)) &&
                        (Double.TryParse(gtotalTextBox.Text, out gtotal)) &&
                        (Double.TryParse(latitudeTextBox.Text, out lat)) &&
                        (Double.TryParse(dipTextBox.Text, out dip)) &&
                        (Double.TryParse(decTextBox.Text, out dec)))

                        wellRefData = new WellReferenceData(btotal,
                        new Declination(dec, AngleUnit.Degrees),
                        new Angle(dip, AngleUnit.Degrees),
                        new Angle(lat, AngleUnit.Degrees),
                         gtotal);

                    else
                    {
                        MessageBox.Show("Error Reading Well Parameters");
                        return;
                    }
                }

                int toolCodeId = Convert.ToInt32(toolCodeTextBox.Text);

                List<SurveyMeasurement> wellSurveyMeasurements = new List<SurveyMeasurement>();
                StreamReader reader = new StreamReader(wellPathFileName);
                while (!reader.EndOfStream)
                {
                    string inputLine = reader.ReadLine();
                    if (inputLine != "")
                    {
                        string[] inputFields = inputLine.Split(new char[] { ' ', ',', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                        if ((bool)gridAzimuthCheckBox.IsChecked)
                            wellSurveyMeasurements.Add(new SurveyMeasurement(
                                    new MeasuredDepth(Convert.ToDouble(inputFields[0]), DistanceUnit.Metres),
                                    new Inclination(Convert.ToDouble(inputFields[1]), AngleUnit.Degrees),
                                    new GridAzimuth(Convert.ToDouble(inputFields[2]), AngleUnit.Degrees),
                                    wellRefData.Declination,
                                    new Convergence(Convert.ToDouble(convergenceTextBox.Text), AngleUnit.Degrees)));
                        else
                            wellSurveyMeasurements.Add(new SurveyMeasurement(
                                    new MeasuredDepth(Convert.ToDouble(inputFields[0]), DistanceUnit.Metres),
                                    new Inclination(Convert.ToDouble(inputFields[1]), AngleUnit.Degrees),
                                    new TrueAzimuth(Convert.ToDouble(inputFields[2]), AngleUnit.Degrees),
                                    wellRefData.Declination,
                                    new Convergence(Convert.ToDouble(convergenceTextBox.Text), AngleUnit.Degrees)));
                    }
                }

                Survey wellPath = new Survey(slot, wellSurveyMeasurements);
                List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, toolCodeId) };

                // List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, toolCodeId), new TieOn(1800, toolCodeId), new TieOn(2931, toolCodeId), new TieOn(4137, toolCodeId), new TieOn(7100, toolCodeId), new TieOn(7650, toolCodeId) };
                // List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, toolCodeId) , new TieOn(440, toolCodeId), new TieOn(2020, toolCodeId), new TieOn(2683, toolCodeId), new TieOn(3088, toolCodeId) };
                // Total - List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, 1), new TieOn(211, 3), new TieOn(1291, 7) };
                // List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, 0), new TieOn(75.00, 9) };
                // List<TieOn> wellSurveyProgram = new List<TieOn> { new TieOn(0, 0), new TieOn(1000, 53), new TieOn(2000, 0), new TieOn(3500, 0), new TieOn(5000, 53) };  // Test IncOnly

                string toolCodeDatabaseLocation = @"C:\Users\Andy34479\Documents\AJCErrorModel\";
                string errorSourceDatabaseFile = @"C:\Users\Andy34479\Documents\AJCErrorModel\AJC_ErrorSourceDatabase.xml";
                List<Matrix3x3> wellNEVCovariances = new List<Matrix3x3>();
                ISCWSAErrorModel primaryWellErrors = new ISCWSAErrorModel(wellPath, wellRefData, wellSurveyProgram, XMLReader.ReadToolCodes(toolCodeDatabaseLocation, errorSourceDatabaseFile));
                primaryWellErrors.ReadMultipleDatabaseFiles = true;
                

                primaryWellErrors.EnforceInclinationBounds = false;
                if (surfaceTieOnTickBox.IsChecked == true)
                    primaryWellErrors.HandleSurfaceTieOn = true;
                else
                    primaryWellErrors.HandleSurfaceTieOn = false;
                if (gridCovarianceCheckBox.IsChecked == true) primaryWellErrors.GridReferencedCovariances = true;
                if (diagnosticsTickBox.IsChecked == true)
                    primaryWellErrors.WriteDiagnostics = true;
                if (verboseCheckBox.IsChecked == true) primaryWellErrors.Verbose = true;
                primaryWellErrors.HLADiagnostics = true;

                primaryWellErrors.Calculate();

                int i = 0;
                outputTextBox.AppendText("\r\nNEV Covariance Matrix =====================================");
                outputTextBox.AppendText("\r\n          MD          NN           EE            VV            NE             NV           EV\r\n");

                foreach (var covariance in primaryWellErrors.NEVCovariances)
                {
                    outputTextBox.AppendText(string.Format("{0,14:F2}",Convert.ToDouble(wellSurveyMeasurements[i++].Md))  + CovarianceToString(covariance)+"\n");
                }

                outputTextBox.AppendText("\r\nTD HLA Correlation Matrix =====================================\r\n");
                outputTextBox.AppendText("          sigH          sigL           sigAH            HL            HA             LA           EV\r\n");
                i--;
                double[] correlation = primaryWellErrors.GetHLACorrelation(i);
                outputTextBox.AppendText(string.Format("{0,14:F2}{1,12:F4}{2,12:F4}{3,12:F4}{4,12:F4}{5,12:F4}{6,12:F4}", Convert.ToDouble(wellSurveyMeasurements[i].Md),
                correlation[0], correlation[1], correlation[2], correlation[3], correlation[4], correlation[5]));


                outputTextBox.AppendText("\r\n\r\nSurvey Measurement Errors =====================================\r\n");
                outputTextBox.AppendText("                      MD                      Depth                        Inc                           Az\r\n");
                i = 0;
                foreach (var measurementUncertainty in primaryWellErrors.MeasurementUncertainties)
                {
                    outputTextBox.AppendText(string.Format("{0,26:F2}{1,26:F6}{2,26:F6}{3,26:F6}", Convert.ToDouble(wellSurveyMeasurements[i++].Md), measurementUncertainty[0], measurementUncertainty[1], measurementUncertainty[2])+"\r\n");

                }

                if ((bool)CompassReportCheckBox.IsChecked)
                    WriteCompassDimensions(primaryWellErrors);
                else                    
                    WriteEllipsoidDimensions(primaryWellErrors);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error Model Calling Routine: \n\r" + ex.Message, "AJC Error Model");
            }
        }

        void WriteCompassDimensions(ISCWSAErrorModel primaryWellErrors)
        {
            try
            {
                StreamWriter sw = new StreamWriter(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AJCErrorModel\\Diagnostics\\WellEllipses.txt");

                List<CompassUncertainty> ellipsoids = new List<CompassUncertainty>();


                double stdDev = 1.0;
                Double.TryParse(stdDevTextBox.Text, out stdDev);
                SurfaceUncertainty surfaceUncertainty = new SurfaceUncertainty();

                ellipsoids = UncertaintyReportGenerator.GetCompassUncertaintyData(primaryWellErrors.NEVCovariances, 0, primaryWellErrors.NEVCovariances.Count,
                                                  primaryWellErrors.WellSurvey, surfaceUncertainty, stdDev);

                string spacer = "\t";
                string header = spacer + "Md" + spacer + "HS" + spacer + "Lat" + spacer + "Vert" + spacer  + "SMajor" + spacer + "SMinor" + spacer + "Az";
                sw.WriteLine(header);

                for (int i = 0; i < ellipsoids.Count; i++)
                {
                    sw.WriteLine(String.Format("{0,12:F4}", primaryWellErrors.WellSurvey.Stations[i].Measurement.Md) + ellipsoids[i].ToString());
                }

                sw.Close();
                int ni = ellipsoids.Count - 1;
                outputTextBox.Text = header + "\n\r" + String.Format("{0,12:F4}", primaryWellErrors.WellSurvey.Stations[ni].Measurement.Md) + ellipsoids[ni].ToString() + "\n\r" + outputTextBox.Text;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("GUI Compass Uncertainty Error: \n\r" + ex.Message, "AJC Error Model");
            }
        }


        void WriteEllipsoidDimensions(ISCWSAErrorModel primaryWellErrors)
        {
            StreamWriter sw = new StreamWriter(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\AJCErrorModel\\Diagnostics\\WellEllipses.txt");
            
            List<EllipsoidDimensions> ellipsoids = new List<EllipsoidDimensions>();

            bool projectEllipsoid = (bool)projectEllipsoidCheckBox.IsChecked;
            bool HLAAxes = (bool)HLAAxesCheckBox.IsChecked;
            bool matchCompass = (bool)matchCompassCheckBox.IsChecked;
            double stdDev = 1.0;
            Double.TryParse(stdDevTextBox.Text, out stdDev);
            bool includeSurfaceUncertainty = false;
            bool reportEllipse = (bool)createEllipseCheckBox.IsChecked;
            UncertaintyReportOptions options = new UncertaintyReportOptions(includeSurfaceUncertainty,projectEllipsoid, HLAAxes, matchCompass, stdDev, reportEllipse);
            SurfaceUncertainty surfaceUncertainty = new SurfaceUncertainty();

            ellipsoids = UncertaintyReportGenerator.GetEllipseData(primaryWellErrors.NEVCovariances, 0, primaryWellErrors.NEVCovariances.Count,
                                              primaryWellErrors.WellSurvey, options, surfaceUncertainty);

            string spacer = "\t";
            sw.WriteLine("\t\tMd" + spacer + ellipsoids[0].SemiAxis1.Label + spacer + ellipsoids[0].SemiAxis2.Label + spacer + ellipsoids[0].SemiAxis3.Label + spacer +
                ellipsoids[0].OrientationAngle1.Label + spacer + ellipsoids[0].OrientationAngle2.Label + spacer + ellipsoids[0].OrientationAngle3.Label);

            for (int i = 0; i < ellipsoids.Count; i++)
            {
                sw.WriteLine(String.Format("{0,12:F4}",primaryWellErrors.WellSurvey.Stations[i].Measurement.Md) + ellipsoids[i].ToString());
            }

            sw.Close();
         }

        string CovarianceToString(Matrix3x3 cov)
        { 
            return String.Format("{0,12:F4}{1,12:F4}{2,12:F4}{3,12:F4}{4,12:F4}{5,12:F4}", cov[0, 0], cov[1,1], cov[2,2], cov[0,1], cov[0,2], cov[1,2]); 
        }

        private void UserRadio_Checked(object sender, RoutedEventArgs e)
        {
            wellParametersGroupBox.Visibility = Visibility.Visible;
        }

        private void Well1Radio_Checked(object sender, RoutedEventArgs e)
        {
            wellParametersGroupBox.Visibility = Visibility.Hidden;
        }

        private void Well2Radio_Checked(object sender, RoutedEventArgs e)
        {
            wellParametersGroupBox.Visibility = Visibility.Hidden;
        }

        private void Well3Radio_Checked(object sender, RoutedEventArgs e)
        {
            wellParametersGroupBox.Visibility = Visibility.Hidden;
        }

        private void GridCovarianceCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CreateEllipseCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
